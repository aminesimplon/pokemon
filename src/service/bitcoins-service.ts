import type { bitcoins } from "@/entities";
import axios from "axios";

export async function fetchBitcoins(){
    const response = await axios.get<bitcoins[]>('https://api.coincap.io/v2/assets/bitcoin/history?interval=d1')
    return response.data;
}