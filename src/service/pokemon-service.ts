import type { pokemon } from "@/entities";
import axios from "axios";

export async function fetchPokemon(){
    const response = await axios.get<pokemon[]>('https://pokebuildapi.fr/api/v1/pokemon')
    return response.data;
}