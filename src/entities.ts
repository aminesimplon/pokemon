export interface pokemon {
    "id": Number,
    "name": String,
    "image": String
    "apiTypes": [
        { "name": String }
    ]
}

export interface bitcoins {
    "data": [
        {
            "priceUsd": String,
            "time": Number
        }]
}